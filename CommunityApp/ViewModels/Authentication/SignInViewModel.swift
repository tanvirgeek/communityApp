//
//  SignInViewModel.swift
//  CommunityApp
//
//  Created by Tanvir Alam on 10/9/21.
//

import Foundation

class SignInViewModel:ObservableObject{
    @Published var fullName = ""
    @Published var gender = ""
    @Published var positionInTheCommitte = BanglaStrings.commityOptions[0]
    @Published var district = BanglaStrings.districtOptions[0]
    @Published var NID = ""
}
