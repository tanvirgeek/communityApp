//
//  BanglaStrings.swift
//  CommunityApp
//
//  Created by Tanvir Alam on 9/9/21.
//

import Foundation

class BanglaStrings{
    static let titleBarSignIn = "নিবন্ধন করুন"
    static let nameHeader = "সম্পূর্ণ নাম"
    static let genderHeader = "লিঙ্গ নির্বাচন করুন"
    static let commityPositionHeader = "বর্তমান কমিটিতে আপনার অবস্থান কি?"
    static let district = "জেলা"
    static let NID = "জাতীয় পরিচয়পত্রের নম্বর"
    static let nextButton = "পরবর্তী"
    static let signedIn = "নিবন্ধিত আছেন?"
    static let signIn = "সাইন ইন"
    static let placeHolderName = "আপনার সম্পূর্ণ নাম লিখুন"
    static let commityOptions:[String] = ["সভাপতি"]
    static let districtOptions:[String] = ["ঢাকা"]
    
}
