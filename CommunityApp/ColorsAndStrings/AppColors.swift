//
//  AppColors.swift
//  CommunityApp
//
//  Created by Tanvir Alam on 9/9/21.
//

import Foundation
import SwiftUI

class AppColors{
    static let mainGreen: Color = Color(#colorLiteral(red: 0, green: 0.6509803922, blue: 0.3176470588, alpha: 1)) // 00A651
    static let titleBarWhiteColor:Color = Color(#colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1)) //F4F4F4
    static let headerGreyColor = Color(#colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)) //4F4F4F
    static let backGroudColor = Color(#colorLiteral(red: 0.9646111131, green: 0.964772284, blue: 0.9645897746, alpha: 1)) //F6F6F6
    static let lightGreen = Color(#colorLiteral(red: 0.3850449324, green: 0.7777624726, blue: 0.5770658255, alpha: 1)) // 7CC497
    static let lightBlue = Color(#colorLiteral(red: 0.2185923457, green: 0.264154315, blue: 0.6464139819, alpha: 1)) //3A439F
}

