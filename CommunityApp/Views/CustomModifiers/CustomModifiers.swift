//
//  CustomModifiers.swift
//  CommunityApp
//
//  Created by Tanvir Alam on 10/9/21.
//

import Foundation
import SwiftUI


// HeaderText Style

struct HeaderTextStyle: ViewModifier {
    func body(content: Content) -> some View {
        content
            .foregroundColor(AppColors.headerGreyColor)
            .font(.system(size: 16,weight: .medium))
    }
}

extension View {
    func headerTextStyle() -> some View {
        self.modifier(HeaderTextStyle())
    }
}
