//
//  Admin2View.swift
//  CommunityApp
//
//  Created by Tanvir Alam on 9/9/21.
//

import SwiftUI

struct Admin2View: View {
    @ObservedObject private var signinVM = SignInViewModel()
    var body: some View {
        VStack{
            Image("addProfileImage")
            
            Form{
                Section(header:Text(BanglaStrings.nameHeader).headerTextStyle()) {
                    TextField(BanglaStrings.placeHolderName, text: $signinVM.fullName)
                }
                
                Section(header:Text(BanglaStrings.genderHeader).headerTextStyle()) {
                    Text("This is gender picker")
                }
                
                Section(header: Text(BanglaStrings.commityPositionHeader).headerTextStyle()) {
                    Picker("", selection: $signinVM.positionInTheCommitte) {
                        ForEach(BanglaStrings.commityOptions, id: \.self) {
                            Text($0)
                        }
                    }
                }
                
                Section(header: Text(BanglaStrings.district).headerTextStyle()) {
                    Picker("", selection: $signinVM.district) {
                        ForEach(BanglaStrings.districtOptions, id: \.self) {
                            Text($0)
                        }
                    }
                }
                
                Section(header: Text(BanglaStrings.NID).headerTextStyle()) {
                    TextField("348756746543", text: $signinVM.NID)
                }
            }
            
            NavigationLink(
                destination: Text("This is the sign up page two"),
                label: {
                    Text(BanglaStrings.nextButton)
                        .background(Color.green)
                })
            
            HStack{
                Text(BanglaStrings.signedIn)
                Text(BanglaStrings.signIn)
            }

        }
    }
}

struct Admin2View_Previews: PreviewProvider {
    static var previews: some View {
        Admin2View()
    }
}
