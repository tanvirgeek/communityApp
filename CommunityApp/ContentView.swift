//
//  ContentView.swift
//  CommunityApp
//
//  Created by Tanvir Alam on 9/9/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        
        NavigationView{
            Admin2View()
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
